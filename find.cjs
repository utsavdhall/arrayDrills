function find(items,cb){
    if(items==undefined){
        return -1;
    }
    for(let item of items){
        let x=cb(item);
        if(x==true){
            return item;
        }
    }
}

module.exports=find;