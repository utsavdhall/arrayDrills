function filter(items,cb){
    let filtered=[];
    if(items==undefined){
        return filtered;
    }
    for(let index=0;index<items.length;index++){
        let filteredElement=cb(items[index],index,items);
        if(filteredElement==true){
            filtered.push(items[index]);
        }
    }
    return filtered;
}

module.exports=filter;