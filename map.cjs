function map(items,cb){
    let mapped=[];
    if(items==undefined){
        return mapped;
    }
    for(let index=0;index<items.length;index++){
        let modifiedItem=cb(items[index],index,items);
        mapped.push(modifiedItem);
    }
    return mapped;
}

module.exports=map;