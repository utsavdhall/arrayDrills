function each(items,cb){
    if(items==undefined) return;
    for(let i=0;i<items.length;i++){
        items[i]=cb(items[i]);
    }
}

module.exports=each;