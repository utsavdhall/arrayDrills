const each =require("../forEach.cjs");
const items=  require("./items.cjs");
function eachTest(){
    let ourItems=items();
    each(ourItems,(item)=>{
        return item*10;
    });
    console.log(ourItems);
}
eachTest();