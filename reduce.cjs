function reduce(items,cb,initialValue){
    let index = 0;
    if (initialValue === undefined) {
        initialValue = items[0];
        index = 1
    }
    while (index<items.length){
        initialValue = cb(initialValue,items[index],index,items);
        index++;
    }
    return initialValue;

}

module.exports=reduce;